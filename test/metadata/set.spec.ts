import { Metadata } from '../../src';
//
//
//
test('constructor', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent });
    Metadata.set('parentMetadata', 'parent', { target: Parent });
    Metadata.set('metadata', 'child', { target: Child });
    Metadata.set('childMetadata', 'child', { target: Child });

    expect(Metadata.getOwn('metadata', { target: Parent })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child })).toBe('child');
    expect(() => Metadata.get('never', { target: Child })).toThrow('Key does not exist.');
});
test('constructor-parameter', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent, parameterIndex: 0 });
    Metadata.set('parentMetadata', 'parent', { target: Parent, parameterIndex: 0 });
    Metadata.set('metadata', 'child', { target: Child, parameterIndex: 0 });
    Metadata.set('childMetadata', 'child', { target: Child, parameterIndex: 0 });

    expect(Metadata.getOwn('metadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, parameterIndex: 0 })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, parameterIndex: 0 })).toThrow('Key does not exist.');
});

test('static-property', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent, propertyKey: 'staticProperty' });
    Metadata.set('parentMetadata', 'parent', { target: Parent, propertyKey: 'staticProperty' });
    Metadata.set('metadata', 'child', { target: Child, propertyKey: 'staticProperty' });
    Metadata.set('childMetadata', 'child', { target: Child, propertyKey: 'staticProperty' });

    expect(Metadata.getOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');
});
test('static-method', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent, propertyKey: 'staticMethod' });
    Metadata.set('parentMetadata', 'parent', { target: Parent, propertyKey: 'staticMethod' });
    Metadata.set('metadata', 'child', { target: Child, propertyKey: 'staticMethod' });
    Metadata.set('childMetadata', 'child', { target: Child, propertyKey: 'staticMethod' });

    expect(Metadata.getOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');
});
test('static-method-parameter', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 });
    Metadata.set('parentMetadata', 'parent', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 });
    Metadata.set('metadata', 'child', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 });
    Metadata.set('childMetadata', 'child', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 });

    expect(Metadata.getOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
});

test('instance-property', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent.prototype, propertyKey: 'instanceProperty' });
    Metadata.set('parentMetadata', 'parent', { target: Parent.prototype, propertyKey: 'instanceProperty' });
    Metadata.set('metadata', 'child', { target: Child.prototype, propertyKey: 'instanceProperty' });
    Metadata.set('childMetadata', 'child', { target: Child.prototype, propertyKey: 'instanceProperty' });

    expect(Metadata.getOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');
});
test('instance-method', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent.prototype, propertyKey: 'instanceMethod' });
    Metadata.set('parentMetadata', 'parent', { target: Parent.prototype, propertyKey: 'instanceMethod' });
    Metadata.set('metadata', 'child', { target: Child.prototype, propertyKey: 'instanceMethod' });
    Metadata.set('childMetadata', 'child', { target: Child.prototype, propertyKey: 'instanceMethod' });

    expect(Metadata.getOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');
});
test('instance-method-parameter', () => {
    class Parent { }

    class Child extends Parent { }

    Metadata.set('metadata', 'parent', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });
    Metadata.set('parentMetadata', 'parent', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });
    Metadata.set('metadata', 'child', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });
    Metadata.set('childMetadata', 'child', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });

    expect(Metadata.getOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.get('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
});
