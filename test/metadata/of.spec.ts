import { Metadata } from '../../src';
//
//
//
test('invalid', () => {
    class Target { }

    expect(() => Metadata.of({ target: Function })).toThrow('Invalid target');
    expect(() => Metadata.of({ target: Function.prototype })).toThrow('Invalid target');

    expect(() => Metadata.of({ target: Target, parameterIndex: - 1 })).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target, parameterIndex: 0.1 })).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target, propertyKey: 'staticMethod', parameterIndex: - 1 })).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target, propertyKey: 'staticMethod', parameterIndex: 0.1 })).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target.prototype, propertyKey: 'instanceMethod', parameterIndex: - 1 })).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target.prototype, propertyKey: 'instanceMethod', parameterIndex: 0.1 })).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target }).of(-1)).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target }).of(0.1)).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target, propertyKey: 'staticMethod' }).of(-1)).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target, propertyKey: 'staticMethod' }).of(0.1)).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target.prototype, propertyKey: 'instanceMethod' }).of(-1)).toThrow('Invalid parameter index.');
    expect(() => Metadata.of({ target: Target.prototype, propertyKey: 'instanceMethod' }).of(0.1)).toThrow('Invalid parameter index.');

    expect(() => Metadata.of({ target: Target, parameterIndex: 0 }).of(0)).toThrow('Parameter metadata can only be used in constructor or method metadata.');
    expect(() => Metadata.of({ target: Target, propertyKey: 'staticMethod', parameterIndex: 0 }).of(0)).toThrow('Parameter metadata can only be used in constructor or method metadata.');
    expect(() => Metadata.of({ target: Target.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 }).of(0)).toThrow('Parameter metadata can only be used in constructor or method metadata.');
});

test('constructor', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent });
    const childMetadata: Metadata = Metadata.of({ target: Child });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});
test('constructor-parameter', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent, parameterIndex: 0 });
    const childMetadata: Metadata = Metadata.of({ target: Child, parameterIndex: 0 });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});

test('static-property', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent, propertyKey: 'staticProperty' });
    const childMetadata: Metadata = Metadata.of({ target: Child, propertyKey: 'staticProperty' });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});
test('static-method', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent, propertyKey: 'staticMethod' });
    const childMetadata: Metadata = Metadata.of({ target: Child, propertyKey: 'staticMethod' });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});
test('static-method-parameter', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 });
    const childMetadata: Metadata = Metadata.of({ target: Child, propertyKey: 'staticMethod', parameterIndex: 0 });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});

test('instance-property', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent.prototype, propertyKey: 'instanceProperty' });
    const childMetadata: Metadata = Metadata.of({ target: Child.prototype, propertyKey: 'instanceProperty' });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});
test('instance-method', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent.prototype, propertyKey: 'instanceMethod' });
    const childMetadata: Metadata = Metadata.of({ target: Child.prototype, propertyKey: 'instanceMethod' });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});
test('instance-method-parameter', () => {
    class Parent { }

    class Child extends Parent { }

    const parentMetadata: Metadata = Metadata.of({ target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });
    const childMetadata: Metadata = Metadata.of({ target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });

    expect(parentMetadata.parent).toBe(null);
    expect(childMetadata.parent).toBe(parentMetadata);
});
