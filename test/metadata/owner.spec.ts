import { Metadata } from '../../src';
//
//
//
class Target { }

test('constructor', () => {
    expect(Metadata.of({
        target: Target
    }).owner).toEqual({
        target: Target,
        propertyKey: undefined,
        parameterIndex: undefined
    });
});
test('constructor-parameter', () => {
    expect(Metadata.of({
        target: Target,
        parameterIndex: 0
    }).owner).toEqual({
        target: Target,
        propertyKey: undefined,
        parameterIndex: 0
    });
});

test('static-property', () => {
    expect(Metadata.of({
        target: Target,
        propertyKey: 'staticProperty'
    }).owner).toEqual({
        target: Target,
        propertyKey: 'staticProperty',
        parameterIndex: undefined
    });
});
test('static-method', () => {
    expect(Metadata.of({
        target: Target,
        propertyKey: 'staticMethod'
    }).owner).toEqual({
        target: Target,
        propertyKey: 'staticMethod',
        parameterIndex: undefined
    });
});
test('static-method-parameter', () => {
    expect(Metadata.of({
        target: Target,
        propertyKey: 'staticMethod',
        parameterIndex: 0
    }).owner).toEqual({
        target: Target,
        propertyKey: 'staticMethod',
        parameterIndex: 0
    });
});

test('instance-property', () => {
    expect(Metadata.of({
        target: Target.prototype,
        propertyKey: 'instanceProperty'
    }).owner).toEqual({
        target: Target.prototype,
        propertyKey: 'instanceProperty',
        parameterIndex: undefined
    });
});
test('instance-method', () => {
    expect(Metadata.of({
        target: Target.prototype,
        propertyKey: 'instanceMethod'
    }).owner).toEqual({
        target: Target.prototype,
        propertyKey: 'instanceMethod',
        parameterIndex: undefined
    });
});
test('instance-method-parameter', () => {
    expect(Metadata.of({
        target: Target.prototype,
        propertyKey: 'instanceMethod',
        parameterIndex: 0
    }).owner).toEqual({
        target: Target.prototype,
        propertyKey: 'instanceMethod',
        parameterIndex: 0
    });
});
