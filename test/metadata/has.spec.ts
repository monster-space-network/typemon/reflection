import { Metadata } from '../../src';
//
//
//
test('constructor', () => {
    @Metadata.Define('metadata', 'parent')
    @Metadata.Define('parentMetadata', 'parent')
    class Parent { }

    @Metadata.Define('metadata', 'child')
    @Metadata.Define('childMetadata', 'child')
    class Child extends Parent { }

    expect(Metadata.hasOwn('metadata', { target: Parent })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child })).toBeFalsy();
    expect(Metadata.has('never', { target: Child })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child })).toBeTruthy();
});
test('constructor-parameter', () => {
    class Parent {
        public constructor(
            @Metadata.Define('metadata', 'parent')
            @Metadata.Define('parentMetadata', 'parent')
            constructorParameter: unknown
        ) { }
    }

    class Child extends Parent {
        public constructor(
            @Metadata.Define('metadata', 'child')
            @Metadata.Define('childMetadata', 'child')
            constructorParameter: unknown
        ) {
            super(constructorParameter);
        }
    }

    expect(Metadata.hasOwn('metadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent, parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent, parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child, parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child, parameterIndex: 0 })).toBeTruthy();
});

test('static-property', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public static staticProperty: unknown;
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public static staticProperty: unknown;
    }

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.has('never', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
});
test('static-method', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public static staticMethod(): void { }
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public static staticMethod(): void { }
    }

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.has('never', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
});
test('static-method-parameter', () => {
    class Parent {
        public static staticMethod(
            @Metadata.Define('metadata', 'parent')
            @Metadata.Define('parentMetadata', 'parent')
            staticMethodParameter: unknown
        ): void { }
    }

    class Child extends Parent {
        public static staticMethod(
            @Metadata.Define('metadata', 'child')
            @Metadata.Define('childMetadata', 'child')
            staticMethodParameter: unknown
        ): void { }
    }

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
});

test('instance-property', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public instanceProperty: unknown;
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public instanceProperty: unknown;
    }

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.has('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
});
test('instance-method', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public instanceMethod(): void { }
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public instanceMethod(): void { }
    }

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.has('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
});
test('instance-method-parameter', () => {
    class Parent {
        public instanceMethod(
            @Metadata.Define('metadata', 'parent')
            @Metadata.Define('parentMetadata', 'parent')
            instanceMethodParameter: unknown
        ): void { }
    }

    class Child extends Parent {
        public instanceMethod(
            @Metadata.Define('metadata', 'child')
            @Metadata.Define('childMetadata', 'child')
            instanceMethodParameter: unknown
        ): void { }
    }

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
});
