import { Metadata } from '../../src';
//
//
//
test('constructor', () => {
    @Metadata.Define('metadata', 'parent')
    class Parent { }

    @Metadata.Define('metadata', 'child')
    class Child extends Parent { }

    Metadata.delete('metadata', { target: Child });

    expect(Metadata.hasOwn('metadata', { target: Parent })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent });

    expect(Metadata.hasOwn('metadata', { target: Parent })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child })).toBeTruthy();
});

test('constructor', () => {
    class Parent {
        public constructor(
            @Metadata.Define('metadata', 'parent')
            constructorParameter: unknown
        ) { }
    }

    class Child extends Parent {
        public constructor(
            @Metadata.Define('metadata', 'child')
            constructorParameter: unknown
        ) {
            super(constructorParameter);
        }
    }

    Metadata.delete('metadata', { target: Child, parameterIndex: 0 });

    expect(Metadata.hasOwn('metadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, parameterIndex: 0 })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent, parameterIndex: 0 });

    expect(Metadata.hasOwn('metadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent, parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child, parameterIndex: 0 })).toBeTruthy();
});

test('static-property', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        public static staticProperty: unknown;
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        public static staticProperty: unknown;
    }

    Metadata.delete('metadata', { target: Child, propertyKey: 'staticProperty' });

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent, propertyKey: 'staticProperty' });

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticProperty' })).toBeTruthy();
});
test('static-method', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        public static staticMethod(): void { }
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        public static staticMethod(): void { }
    }

    Metadata.delete('metadata', { target: Child, propertyKey: 'staticMethod' });

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent, propertyKey: 'staticMethod' });

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticMethod' })).toBeTruthy();
});
test('static-method-parameter', () => {
    class Parent {
        public static staticMethod(
            @Metadata.Define('metadata', 'parent')
            staticMethodParameter: unknown
        ): void { }
    }

    class Child extends Parent {
        public static staticMethod(
            @Metadata.Define('metadata', 'child')
            staticMethodParameter: unknown
        ): void { }
    }

    Metadata.delete('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 });

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 });

    expect(Metadata.hasOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
});

test('instance-property', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        public instanceProperty: unknown;
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        public instanceProperty: unknown;
    }

    Metadata.delete('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' });

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' });

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
});
test('instance-method', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        public instanceMethod(): void { }
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        public instanceMethod(): void { }
    }

    Metadata.delete('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' });

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' });

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
});
test('instance-method-parameter', () => {
    class Parent {
        public instanceMethod(
            @Metadata.Define('metadata', 'parent')
            instanceMethodParameter: unknown
        ): void { }
    }

    class Child extends Parent {
        public instanceMethod(
            @Metadata.Define('metadata', 'child')
            instanceMethodParameter: unknown
        ): void { }
    }

    Metadata.delete('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();

    Metadata.delete('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 });

    expect(Metadata.hasOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();

    expect(Metadata.hasOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNotOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.has('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
});
