import { Metadata } from '../../src';
//
//
//
test('constructor', () => {
    @Metadata.Define('metadata', 'parent')
    @Metadata.Define('parentMetadata', 'parent')
    class Parent { }

    @Metadata.Define('metadata', 'child')
    @Metadata.Define('childMetadata', 'child')
    class Child extends Parent { }

    expect(Metadata.getOwn('metadata', { target: Parent })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child })).toBe('child');
    expect(() => Metadata.get('never', { target: Child })).toThrow('Key does not exist.');
});
test('constructor-parameter', () => {
    class Parent {
        public constructor(
            @Metadata.Define('metadata', 'parent')
            @Metadata.Define('parentMetadata', 'parent')
            constructorParameter: unknown
        ) { }
    }

    class Child extends Parent {
        public constructor(
            @Metadata.Define('metadata', 'child')
            @Metadata.Define('childMetadata', 'child')
            constructorParameter: unknown
        ) {
            super(constructorParameter);
        }
    }

    expect(Metadata.getOwn('metadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, parameterIndex: 0 })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, parameterIndex: 0 })).toThrow('Key does not exist.');
});

test('static-property', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public static staticProperty: unknown;
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public static staticProperty: unknown;
    }

    expect(Metadata.getOwn('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, propertyKey: 'staticProperty' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, propertyKey: 'staticProperty' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, propertyKey: 'staticProperty' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, propertyKey: 'staticProperty' })).toThrow('Key does not exist.');
});
test('static-method', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public static staticMethod(): void { }
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public static staticMethod(): void { }
    }

    expect(Metadata.getOwn('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, propertyKey: 'staticMethod' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, propertyKey: 'staticMethod' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, propertyKey: 'staticMethod' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, propertyKey: 'staticMethod' })).toThrow('Key does not exist.');
});
test('static-method-parameter', () => {
    class Parent {
        public static staticMethod(
            @Metadata.Define('metadata', 'parent')
            @Metadata.Define('parentMetadata', 'parent')
            staticMethodParameter: unknown
        ): void { }
    }

    class Child extends Parent {
        public static staticMethod(
            @Metadata.Define('metadata', 'child')
            @Metadata.Define('childMetadata', 'child')
            staticMethodParameter: unknown
        ): void { }
    }

    expect(Metadata.getOwn('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.get('never', { target: Child, propertyKey: 'staticMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
});

test('instance-property', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public instanceProperty: unknown;
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public instanceProperty: unknown;
    }

    expect(Metadata.getOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child.prototype, propertyKey: 'instanceProperty' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child.prototype, propertyKey: 'instanceProperty' })).toThrow('Key does not exist.');
});
test('instance-method', () => {
    class Parent {
        @Metadata.Define('metadata', 'parent')
        @Metadata.Define('parentMetadata', 'parent')
        public instanceMethod(): void { }
    }

    class Child extends Parent {
        @Metadata.Define('metadata', 'child')
        @Metadata.Define('childMetadata', 'child')
        public instanceMethod(): void { }
    }

    expect(Metadata.getOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod' })).toBe('child');
    expect(() => Metadata.get('never', { target: Child.prototype, propertyKey: 'instanceMethod' })).toThrow('Key does not exist.');
});
test('instance-method-parameter', () => {
    class Parent {
        public instanceMethod(
            @Metadata.Define('metadata', 'parent')
            @Metadata.Define('parentMetadata', 'parent')
            instanceMethodParameter: unknown
        ): void { }
    }

    class Child extends Parent {
        public instanceMethod(
            @Metadata.Define('metadata', 'child')
            @Metadata.Define('childMetadata', 'child')
            instanceMethodParameter: unknown
        ): void { }
    }

    expect(Metadata.getOwn('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.getOwn('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.getOwn('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('parentMetadata', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(() => Metadata.get('never', { target: Parent.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.getOwn('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
    expect(Metadata.getOwn('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.getOwn('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');

    expect(Metadata.get('metadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(Metadata.get('parentMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('parent');
    expect(Metadata.get('childMetadata', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBe('child');
    expect(() => Metadata.get('never', { target: Child.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toThrow('Key does not exist.');
});
