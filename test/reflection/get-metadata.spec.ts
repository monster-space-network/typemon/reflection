import { Reflection } from '../../src';
//
//
//
test('constructor', () => {
    @Reflection.DefineMetadata('metadata', 'parent')
    @Reflection.DefineMetadata('parentMetadata', 'parent')
    class Parent { }

    @Reflection.DefineMetadata('metadata', 'child')
    @Reflection.DefineMetadata('childMetadata', 'child')
    class Child extends Parent { }

    expect(Reflection.getOwnMetadata('metadata', Parent)).toBe('parent');
    expect(Reflection.getOwnMetadata('parentMetadata', Parent)).toBe('parent');
    expect(() => Reflection.getOwnMetadata('never', Parent)).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Parent)).toBe('parent');
    expect(Reflection.getMetadata('parentMetadata', Parent)).toBe('parent');
    expect(() => Reflection.getMetadata('never', Parent)).toThrow('Key does not exist.');

    expect(Reflection.getOwnMetadata('metadata', Child)).toBe('child');
    expect(() => Reflection.getOwnMetadata('parentMetadata', Child)).toThrow('Key does not exist.');
    expect(Reflection.getOwnMetadata('childMetadata', Child)).toBe('child');
    expect(() => Reflection.getOwnMetadata('never', Child)).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Child)).toBe('child');
    expect(Reflection.getMetadata('parentMetadata', Child)).toBe('parent');
    expect(Reflection.getMetadata('childMetadata', Child)).toBe('child');
    expect(() => Reflection.getMetadata('never', Child)).toThrow('Key does not exist.');
});

test('static-property', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public static staticProperty: unknown;
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public static staticProperty: unknown;
    }

    expect(Reflection.getOwnMetadata('metadata', Parent, 'staticProperty')).toBe('parent');
    expect(Reflection.getOwnMetadata('parentMetadata', Parent, 'staticProperty')).toBe('parent');
    expect(() => Reflection.getOwnMetadata('never', Parent, 'staticProperty')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Parent, 'staticProperty')).toBe('parent');
    expect(Reflection.getMetadata('parentMetadata', Parent, 'staticProperty')).toBe('parent');
    expect(() => Reflection.getMetadata('never', Parent, 'staticProperty')).toThrow('Key does not exist.');

    expect(Reflection.getOwnMetadata('metadata', Child, 'staticProperty')).toBe('child');
    expect(() => Reflection.getOwnMetadata('parentMetadata', Child, 'staticProperty')).toThrow('Key does not exist.');
    expect(Reflection.getOwnMetadata('childMetadata', Child, 'staticProperty')).toBe('child');
    expect(() => Reflection.getOwnMetadata('never', Child, 'staticProperty')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Child, 'staticProperty')).toBe('child');
    expect(Reflection.getMetadata('parentMetadata', Child, 'staticProperty')).toBe('parent');
    expect(Reflection.getMetadata('childMetadata', Child, 'staticProperty')).toBe('child');
    expect(() => Reflection.getMetadata('never', Child, 'staticProperty')).toThrow('Key does not exist.');
});
test('static-method', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public static staticMethod(): void { }
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public static staticMethod(): void { }
    }

    expect(Reflection.getOwnMetadata('metadata', Parent, 'staticMethod')).toBe('parent');
    expect(Reflection.getOwnMetadata('parentMetadata', Parent, 'staticMethod')).toBe('parent');
    expect(() => Reflection.getOwnMetadata('never', Parent, 'staticMethod')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Parent, 'staticMethod')).toBe('parent');
    expect(Reflection.getMetadata('parentMetadata', Parent, 'staticMethod')).toBe('parent');
    expect(() => Reflection.getMetadata('never', Parent, 'staticMethod')).toThrow('Key does not exist.');

    expect(Reflection.getOwnMetadata('metadata', Child, 'staticMethod')).toBe('child');
    expect(() => Reflection.getOwnMetadata('parentMetadata', Child, 'staticMethod')).toThrow('Key does not exist.');
    expect(Reflection.getOwnMetadata('childMetadata', Child, 'staticMethod')).toBe('child');
    expect(() => Reflection.getOwnMetadata('never', Child, 'staticMethod')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Child, 'staticMethod')).toBe('child');
    expect(Reflection.getMetadata('parentMetadata', Child, 'staticMethod')).toBe('parent');
    expect(Reflection.getMetadata('childMetadata', Child, 'staticMethod')).toBe('child');
    expect(() => Reflection.getMetadata('never', Child, 'staticMethod')).toThrow('Key does not exist.');
});

test('instance-property', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public instanceProperty: unknown;
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public instanceProperty: unknown;
    }

    expect(Reflection.getOwnMetadata('metadata', Parent.prototype, 'instanceProperty')).toBe('parent');
    expect(Reflection.getOwnMetadata('parentMetadata', Parent.prototype, 'instanceProperty')).toBe('parent');
    expect(() => Reflection.getOwnMetadata('never', Parent.prototype, 'instanceProperty')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Parent.prototype, 'instanceProperty')).toBe('parent');
    expect(Reflection.getMetadata('parentMetadata', Parent.prototype, 'instanceProperty')).toBe('parent');
    expect(() => Reflection.getMetadata('never', Parent.prototype, 'instanceProperty')).toThrow('Key does not exist.');

    expect(Reflection.getOwnMetadata('metadata', Child.prototype, 'instanceProperty')).toBe('child');
    expect(() => Reflection.getOwnMetadata('parentMetadata', Child.prototype, 'instanceProperty')).toThrow('Key does not exist.');
    expect(Reflection.getOwnMetadata('childMetadata', Child.prototype, 'instanceProperty')).toBe('child');
    expect(() => Reflection.getOwnMetadata('never', Child.prototype, 'instanceProperty')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Child.prototype, 'instanceProperty')).toBe('child');
    expect(Reflection.getMetadata('parentMetadata', Child.prototype, 'instanceProperty')).toBe('parent');
    expect(Reflection.getMetadata('childMetadata', Child.prototype, 'instanceProperty')).toBe('child');
    expect(() => Reflection.getMetadata('never', Child.prototype, 'instanceProperty')).toThrow('Key does not exist.');
});
test('instance-method', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public instanceMethod(): void { }
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public instanceMethod(): void { }
    }

    expect(Reflection.getOwnMetadata('metadata', Parent.prototype, 'instanceMethod')).toBe('parent');
    expect(Reflection.getOwnMetadata('parentMetadata', Parent.prototype, 'instanceMethod')).toBe('parent');
    expect(() => Reflection.getOwnMetadata('never', Parent.prototype, 'instanceMethod')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Parent.prototype, 'instanceMethod')).toBe('parent');
    expect(Reflection.getMetadata('parentMetadata', Parent.prototype, 'instanceMethod')).toBe('parent');
    expect(() => Reflection.getMetadata('never', Parent.prototype, 'instanceMethod')).toThrow('Key does not exist.');

    expect(Reflection.getOwnMetadata('metadata', Child.prototype, 'instanceMethod')).toBe('child');
    expect(() => Reflection.getOwnMetadata('parentMetadata', Child.prototype, 'instanceMethod')).toThrow('Key does not exist.');
    expect(Reflection.getOwnMetadata('childMetadata', Child.prototype, 'instanceMethod')).toBe('child');
    expect(() => Reflection.getOwnMetadata('never', Child.prototype, 'instanceMethod')).toThrow('Key does not exist.');

    expect(Reflection.getMetadata('metadata', Child.prototype, 'instanceMethod')).toBe('child');
    expect(Reflection.getMetadata('parentMetadata', Child.prototype, 'instanceMethod')).toBe('parent');
    expect(Reflection.getMetadata('childMetadata', Child.prototype, 'instanceMethod')).toBe('child');
    expect(() => Reflection.getMetadata('never', Child.prototype, 'instanceMethod')).toThrow('Key does not exist.');
});
