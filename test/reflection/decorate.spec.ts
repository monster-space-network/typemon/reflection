import { Reflection, Metadata } from '../../src';
import { Key } from '../../src/key';
//
//
//
interface Onwer {
    readonly target: () => object;
    readonly propertyKey?: Key;
    readonly parameterIndex?: number;
}

function Decorator({ target, propertyKey, parameterIndex }: Onwer): Metadata.Decorator {
    return Metadata.Decorator.create((metadata: Metadata): void => {
        expect(target()).toBe(metadata.owner.target);
        expect(propertyKey).toBe(metadata.owner.propertyKey);
        expect(parameterIndex).toBe(metadata.owner.parameterIndex);
        expect(Metadata.of({ target: target(), propertyKey, parameterIndex })).toBe(Metadata.of({ target: metadata.owner.target, propertyKey: metadata.owner.propertyKey, parameterIndex: metadata.owner.parameterIndex }));
        metadata.set('metadata', null);
    });
}

class Target { }

test('invalid', () => {
    expect(() => Reflection.decorate([], {
        target: Target
    })).toThrow('Provide at least one decorator.');
});

test('constructor', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target
        })
    ], {
        target: Target
    });

    expect(Metadata.has('metadata', { target: Target })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target })).toBeFalsy();
    expect(Metadata.has('never', { target: Target })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target })).toBeTruthy();
});
test('constructor-parameter', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target,
            parameterIndex: 0
        })
    ], {
        target: Target,
        parameterIndex: 0
    });

    expect(Metadata.has('metadata', { target: Target, parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Target, parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target, parameterIndex: 0 })).toBeTruthy();
});

test('static-property', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target,
            propertyKey: 'staticProperty'
        })
    ], {
        target: Target,
        propertyKey: 'staticProperty'
    });

    expect(Metadata.has('metadata', { target: Target, propertyKey: 'staticProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.has('never', { target: Target, propertyKey: 'staticProperty' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target, propertyKey: 'staticProperty' })).toBeTruthy();
});
test('static-method', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target,
            propertyKey: 'staticMethod'
        })
    ], {
        target: Target,
        propertyKey: 'staticMethod'
    });

    expect(Metadata.has('metadata', { target: Target, propertyKey: 'staticMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.has('never', { target: Target, propertyKey: 'staticMethod' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target, propertyKey: 'staticMethod' })).toBeTruthy();
});
test('static-method-parameter', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target,
            propertyKey: 'staticMethod',
            parameterIndex: 0
        })
    ], {
        target: Target,
        propertyKey: 'staticMethod',
        parameterIndex: 0
    });

    expect(Metadata.has('metadata', { target: Target, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Target, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target, propertyKey: 'staticMethod', parameterIndex: 0 })).toBeTruthy();
});

test('instance-property', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target.prototype,
            propertyKey: 'instanceProperty'
        })
    ], {
        target: Target.prototype,
        propertyKey: 'instanceProperty'
    });

    expect(Metadata.has('metadata', { target: Target.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.has('never', { target: Target.prototype, propertyKey: 'instanceProperty' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target.prototype, propertyKey: 'instanceProperty' })).toBeTruthy();
});
test('instance-method', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target.prototype,
            propertyKey: 'instanceMethod'
        })
    ], {
        target: Target.prototype,
        propertyKey: 'instanceMethod'
    });

    expect(Metadata.has('metadata', { target: Target.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.has('never', { target: Target.prototype, propertyKey: 'instanceMethod' })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target.prototype, propertyKey: 'instanceMethod' })).toBeTruthy();
});
test('instance-method-parameter', () => {
    Reflection.decorate([
        Decorator({
            target: (): object => Target.prototype,
            propertyKey: 'instanceMethod',
            parameterIndex: 0
        })
    ], {
        target: Target.prototype,
        propertyKey: 'instanceMethod',
        parameterIndex: 0
    });

    expect(Metadata.has('metadata', { target: Target.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
    expect(Metadata.hasNot('metadata', { target: Target.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.has('never', { target: Target.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeFalsy();
    expect(Metadata.hasNot('never', { target: Target.prototype, propertyKey: 'instanceMethod', parameterIndex: 0 })).toBeTruthy();
});
