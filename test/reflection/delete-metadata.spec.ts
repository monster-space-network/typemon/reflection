import { Reflection } from '../../src';
//
//
//
test('constructor', () => {
    @Reflection.DefineMetadata('metadata', 'parent')
    class Parent { }

    @Reflection.DefineMetadata('metadata', 'child')
    class Child extends Parent { }

    Reflection.deleteMetadata('metadata', Child);

    expect(Reflection.hasOwnMetadata('metadata', Parent)).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent)).toBeFalsy();
    expect(Reflection.hasMetadata('metadata', Parent)).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent)).toBeFalsy();

    expect(Reflection.hasOwnMetadata('metadata', Child)).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child)).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child)).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child)).toBeFalsy();

    Reflection.deleteMetadata('metadata', Parent);

    expect(Reflection.hasOwnMetadata('metadata', Parent)).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent)).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Parent)).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Parent)).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child)).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child)).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child)).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Child)).toBeTruthy();
});

test('static-property', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        public static staticProperty: unknown;
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        public static staticProperty: unknown;
    }

    Reflection.deleteMetadata('metadata', Child, 'staticProperty');

    expect(Reflection.hasOwnMetadata('metadata', Parent, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('metadata', Parent, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent, 'staticProperty')).toBeFalsy();

    expect(Reflection.hasOwnMetadata('metadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child, 'staticProperty')).toBeFalsy();

    Reflection.deleteMetadata('metadata', Parent, 'staticProperty');

    expect(Reflection.hasOwnMetadata('metadata', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Parent, 'staticProperty')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Child, 'staticProperty')).toBeTruthy();
});
test('static-method', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        public static staticMethod(): void { }
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        public static staticMethod(): void { }
    }

    Reflection.deleteMetadata('metadata', Child, 'staticMethod');

    expect(Reflection.hasOwnMetadata('metadata', Parent, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('metadata', Parent, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent, 'staticMethod')).toBeFalsy();

    expect(Reflection.hasOwnMetadata('metadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child, 'staticMethod')).toBeFalsy();

    Reflection.deleteMetadata('metadata', Parent, 'staticMethod');

    expect(Reflection.hasOwnMetadata('metadata', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Parent, 'staticMethod')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Child, 'staticMethod')).toBeTruthy();
});

test('instance-property', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        public instanceProperty: unknown;
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        public instanceProperty: unknown;
    }

    Reflection.deleteMetadata('metadata', Child.prototype, 'instanceProperty');

    expect(Reflection.hasOwnMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeFalsy();

    expect(Reflection.hasOwnMetadata('metadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child.prototype, 'instanceProperty')).toBeFalsy();

    Reflection.deleteMetadata('metadata', Parent.prototype, 'instanceProperty');

    expect(Reflection.hasOwnMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Child.prototype, 'instanceProperty')).toBeTruthy();
});
test('instance-method', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        public instanceMethod(): void { }
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        public instanceMethod(): void { }
    }

    Reflection.deleteMetadata('metadata', Child.prototype, 'instanceMethod');

    expect(Reflection.hasOwnMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeFalsy();

    expect(Reflection.hasOwnMetadata('metadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child.prototype, 'instanceMethod')).toBeFalsy();

    Reflection.deleteMetadata('metadata', Parent.prototype, 'instanceMethod');

    expect(Reflection.hasOwnMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasMetadata('metadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('metadata', Child.prototype, 'instanceMethod')).toBeTruthy();
});
