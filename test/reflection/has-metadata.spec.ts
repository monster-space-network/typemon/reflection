import { Reflection } from '../../src';
//
//
//
test('constructor', () => {
    @Reflection.DefineMetadata('metadata', 'parent')
    @Reflection.DefineMetadata('parentMetadata', 'parent')
    class Parent { }

    @Reflection.DefineMetadata('metadata', 'child')
    @Reflection.DefineMetadata('childMetadata', 'child')
    class Child extends Parent { }

    expect(Reflection.hasOwnMetadata('metadata', Parent)).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent)).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Parent)).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Parent)).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Parent)).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Parent)).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Parent)).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent)).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Parent)).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Parent)).toBeFalsy();
    expect(Reflection.hasMetadata('never', Parent)).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Parent)).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child)).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child)).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Child)).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Child)).toBeTruthy();
    expect(Reflection.hasOwnMetadata('childMetadata', Child)).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('childMetadata', Child)).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Child)).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Child)).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Child)).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child)).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Child)).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Child)).toBeFalsy();
    expect(Reflection.hasMetadata('childMetadata', Child)).toBeTruthy();
    expect(Reflection.hasNotMetadata('childMetadata', Child)).toBeFalsy();
    expect(Reflection.hasMetadata('never', Child)).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Child)).toBeTruthy();
});

test('static-property', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public static staticProperty: unknown;
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public static staticProperty: unknown;
    }

    expect(Reflection.hasOwnMetadata('metadata', Parent, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Parent, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Parent, 'staticProperty')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Parent, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Parent, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Parent, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Parent, 'staticProperty')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasOwnMetadata('childMetadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('childMetadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Child, 'staticProperty')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('childMetadata', Child, 'staticProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('childMetadata', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Child, 'staticProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Child, 'staticProperty')).toBeTruthy();
});
test('static-method', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public static staticMethod(): void { }
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public static staticMethod(): void { }
    }

    expect(Reflection.hasOwnMetadata('metadata', Parent, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Parent, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Parent, 'staticMethod')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Parent, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Parent, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Parent, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Parent, 'staticMethod')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasOwnMetadata('childMetadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('childMetadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Child, 'staticMethod')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('childMetadata', Child, 'staticMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('childMetadata', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Child, 'staticMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Child, 'staticMethod')).toBeTruthy();
});

test('instance-property', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public instanceProperty: unknown;
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public instanceProperty: unknown;
    }

    expect(Reflection.hasOwnMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Parent.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Parent.prototype, 'instanceProperty')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Parent.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Parent.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Parent.prototype, 'instanceProperty')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasOwnMetadata('childMetadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('childMetadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Child.prototype, 'instanceProperty')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('childMetadata', Child.prototype, 'instanceProperty')).toBeTruthy();
    expect(Reflection.hasNotMetadata('childMetadata', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Child.prototype, 'instanceProperty')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Child.prototype, 'instanceProperty')).toBeTruthy();
});
test('instance-method', () => {
    class Parent {
        @Reflection.DefineMetadata('metadata', 'parent')
        @Reflection.DefineMetadata('parentMetadata', 'parent')
        public instanceMethod(): void { }
    }

    class Child extends Parent {
        @Reflection.DefineMetadata('metadata', 'child')
        @Reflection.DefineMetadata('childMetadata', 'child')
        public instanceMethod(): void { }
    }

    expect(Reflection.hasOwnMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Parent.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Parent.prototype, 'instanceMethod')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Parent.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Parent.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Parent.prototype, 'instanceMethod')).toBeTruthy();

    expect(Reflection.hasOwnMetadata('metadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('metadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('parentMetadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('parentMetadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasOwnMetadata('childMetadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotOwnMetadata('childMetadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasOwnMetadata('never', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotOwnMetadata('never', Child.prototype, 'instanceMethod')).toBeTruthy();

    expect(Reflection.hasMetadata('metadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('metadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('parentMetadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('parentMetadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('childMetadata', Child.prototype, 'instanceMethod')).toBeTruthy();
    expect(Reflection.hasNotMetadata('childMetadata', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasMetadata('never', Child.prototype, 'instanceMethod')).toBeFalsy();
    expect(Reflection.hasNotMetadata('never', Child.prototype, 'instanceMethod')).toBeTruthy();
});
