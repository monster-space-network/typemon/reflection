# [4.1.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/4.0.0...4.1.0) (2020-01-18)


### Features

* **reflection:** decoration-target 인터페이스 수출 ([acbb127](https://gitlab.com/monster-space-network/typemon/reflection/commit/acbb127e8e72bbb5bd558b6b6468e9d99f1732d9))
* **reflection:** define-metadata 데코레이터 추가 ([8265c95](https://gitlab.com/monster-space-network/typemon/reflection/commit/8265c95bf010b0bbe046b4da035158491a8b56d6))



# [4.0.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/3.0.0...4.0.0) (2020-01-13)


### Features

* **reflection.decorate:** 정의 변경 ([4da864c](https://gitlab.com/monster-space-network/typemon/reflection/commit/4da864ccb8c0d306de3187f7f2430bec169843cc))



# [3.0.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/2.0.0...3.0.0) (2020-01-12)


### Bug Fixes

* **metadata:** 잘못된 유형 참조 수정 ([4623ef2](https://gitlab.com/monster-space-network/typemon/reflection/commit/4623ef270b2eb495b6cd0adaf91d063c6e097e89))
* **reflection.decorate:** 잘못된 참조 수정 ([e7e62ba](https://gitlab.com/monster-space-network/typemon/reflection/commit/e7e62ba2930137a4b01f43cd9f1d167979b98a17))


### Features

* **metadata:** 재작성 ([11dec0e](https://gitlab.com/monster-space-network/typemon/reflection/commit/11dec0eec488341ecd80976332225f2cdc87112f))
* **reflection:** decorate 재작성, 느낌표 삭제 ([82e349e](https://gitlab.com/monster-space-network/typemon/reflection/commit/82e349e97d6a9a2eb3abb007aea6d97b5c8f4e2c))
* **reflection:** 유형 수출 키워드 삭제 ([c37f01f](https://gitlab.com/monster-space-network/typemon/reflection/commit/c37f01f861c7564b8cc16f4cbea5e5260315450d))
* 의존성 업데이트 ([1d915ea](https://gitlab.com/monster-space-network/typemon/reflection/commit/1d915ea0835348e458324bdda1626a37fc41bbf9))



# [2.0.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/1.4.0...2.0.0) (2019-10-20)


### Features

* **metadata:** create 액세스 한정자 변경 ([4aec14b](https://gitlab.com/monster-space-network/typemon/reflection/commit/4aec14b695424b1148620e0afc8ea56a5b116844))
* key 추가 ([b8ef695](https://gitlab.com/monster-space-network/typemon/reflection/commit/b8ef695e454a8877b5973a7bd8a54f18cf6b1e00))
* **metadata:** decorator 추가 ([2f8a59c](https://gitlab.com/monster-space-network/typemon/reflection/commit/2f8a59ccfa8983d2cc595ed48012df0cf47a9ff7))
* **metadata:** initial-data 삭제 ([77c533c](https://gitlab.com/monster-space-network/typemon/reflection/commit/77c533c93ce7dde17568d06bd0ca56a5acfbb9d8))
* **metadata:** is-metadata 삭제 ([82ec26c](https://gitlab.com/monster-space-network/typemon/reflection/commit/82ec26ce6bf8979360c8dfc919df50e415fb7cf4))
* **metadata:** iterable, clear 기능 삭제 ([0ca192c](https://gitlab.com/monster-space-network/typemon/reflection/commit/0ca192c52a7a7f5abe2be3585aa296cd9543812d))
* **metadata:** own 기능 추가 ([bdb7427](https://gitlab.com/monster-space-network/typemon/reflection/commit/bdb74274c775073d8f7a3941aa06c4cbeada012a))
* **metadata:** owner 추가 ([9e5c9a5](https://gitlab.com/monster-space-network/typemon/reflection/commit/9e5c9a57aaa65a56bed08b3d26d98bd7699d1dcd))
* **metadata:** 메타데이터 키 액세스 한정자 변경 ([9e4f2d4](https://gitlab.com/monster-space-network/typemon/reflection/commit/9e4f2d49a9fac2ab573b8952ba4560d5999a6950))
* metadata-decorator 삭제 ([2107f6d](https://gitlab.com/monster-space-network/typemon/reflection/commit/2107f6dc5b87999712be7033352bc9b7b84fafd4))
* types 삭제 ([b04b628](https://gitlab.com/monster-space-network/typemon/reflection/commit/b04b628a790cc9da3f8c9b652f32ab79e3263394))
* **metadata:** 생성자 액세스 한정자 변경 ([8e81640](https://gitlab.com/monster-space-network/typemon/reflection/commit/8e81640f670aac5e7c65b0539484688070ab2db6))
* **metadata:** 읽기 전용 유형 삭제 ([069c697](https://gitlab.com/monster-space-network/typemon/reflection/commit/069c6975bceb6da6f6497074b7dca5cae0ca6ce5))
* **metadata:** 키 값 변경 ([21c2eaa](https://gitlab.com/monster-space-network/typemon/reflection/commit/21c2eaad25a79b2799ca36c2b3ef92c4366d6636))
* **metadata:** 파라미터 메타데이터 기능 개선 ([9c86a42](https://gitlab.com/monster-space-network/typemon/reflection/commit/9c86a4298d1d3c3e36bcc462b3e21ffe722f3584))
* **metadata-decorator:** 재작성 ([3e0205b](https://gitlab.com/monster-space-network/typemon/reflection/commit/3e0205bd69ad1d203193e8b3e9f52a7cc3eccc3b))
* **reflection:** own 기능 추가 ([a6c1690](https://gitlab.com/monster-space-network/typemon/reflection/commit/a6c16902aaa9ea151ab00d808aafbcd2785aebd5))


### Performance Improvements

* **metadata:** of 개선 ([4dbb62e](https://gitlab.com/monster-space-network/typemon/reflection/commit/4dbb62e3c0d0b7d867ac81dd3daccb4d086cb346))



# [1.4.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/1.3.0...1.4.0) (2019-10-18)


### Features

* 의존성 업데이트 ([b3c7f49](https://gitlab.com/monster-space-network/typemon/reflection/commit/b3c7f49a3947b5141d33d4a0e97ef04efcba1dcb))
* **metadata:** is-metadata 개선 ([e36c871](https://gitlab.com/monster-space-network/typemon/reflection/commit/e36c8717598d88f202b3c21a6b8e04168922c841))
* **metadata-decorator:** context 인터페이스 추가 및 핸들러 유형 변경 ([2d3af27](https://gitlab.com/monster-space-network/typemon/reflection/commit/2d3af27b628950963052a500bf1c1756191d84b3))



# [1.3.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/1.2.0...1.3.0) (2019-09-19)


### Features

* **reflection:** decorate 추가 ([f80a3f5](https://gitlab.com/monster-space-network/typemon/reflection/commit/f80a3f5))



# [1.2.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/1.1.0...1.2.0) (2019-09-19)


### Bug Fixes

* **metadata-decorator.create:**  반환 유형 수정 ([9242db6](https://gitlab.com/monster-space-network/typemon/reflection/commit/9242db6))
* **reflection:** delete-metadata 수정 ([1384355](https://gitlab.com/monster-space-network/typemon/reflection/commit/1384355))


### Features

* **package:** 의존성 추가 및 업데이트 ([2936b06](https://gitlab.com/monster-space-network/typemon/reflection/commit/2936b06))
* **reflection:** decorate 삭제 ([de3a7d1](https://gitlab.com/monster-space-network/typemon/reflection/commit/de3a7d1))
* **reflection:** has-not-metadata 변경 ([7ce0717](https://gitlab.com/monster-space-network/typemon/reflection/commit/7ce0717))
* **types:** key, target-info 추가 ([b05bce5](https://gitlab.com/monster-space-network/typemon/reflection/commit/b05bce5))
* metadata-decorator 추가 ([8b1beda](https://gitlab.com/monster-space-network/typemon/reflection/commit/8b1beda))
* 수출 항목 추가 ([a26b70c](https://gitlab.com/monster-space-network/typemon/reflection/commit/a26b70c))



# [1.1.0](https://gitlab.com/monster-space-network/typemon/reflection/compare/1.0.0...1.1.0) (2019-09-12)


### Features

* **metadata:** 일부 기능 삭제 및 리팩터링, 정적 메서드 추가 ([4206f61](https://gitlab.com/monster-space-network/typemon/reflection/commit/4206f61))
