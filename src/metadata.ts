import { Check } from '@typemon/check';
//
import { Key } from './key';
import { Reflection } from './reflection';
//
//
//
const KEY: unique symbol = Symbol('typemon.reflection.metadata');
const PARAMETERS_METADATA_KEY: unique symbol = Symbol('typemon.reflection.metadata.parameters-metadata');

export class Metadata {
    private static getMetadata(target: object, propertyKey?: Key): Metadata {
        if (Reflection.hasOwnMetadata(KEY, target, propertyKey)) {
            return Reflection.getOwnMetadata(KEY, target, propertyKey);
        }

        const constructor: Function = Check.isNotFunction(target)
            ? target.constructor
            : target;
        const parentConstructor: Function = Object.getPrototypeOf(constructor);
        const parent: null | object = Check.notEqual(parentConstructor, Function.prototype)
            ? Check.isNotFunction(target)
                ? parentConstructor.prototype
                : parentConstructor
            : null;
        const parentMetadata: null | Metadata = Check.isNotNull(parent)
            ? this.getMetadata(parent, propertyKey)
            : null;
        const metadata: Metadata = new Metadata({ target, propertyKey }, parentMetadata);

        Reflection.setMetadata(KEY, metadata, target, propertyKey);

        return metadata;
    }

    public static of({ target, propertyKey, parameterIndex }: Metadata.Owner): Metadata {
        if (Check.equal(target, Function) || Check.equal(target, Function.prototype)) {
            throw new Error('Invalid target.');
        }

        const metadata: Metadata = this.getMetadata(target, propertyKey);

        if (Check.isUndefined(parameterIndex)) {
            return metadata;
        }

        return metadata.of(parameterIndex);
    }

    public static hasOwn(key: Key, onwer: Metadata.Owner): boolean {
        return this.of(onwer).hasOwn(key);
    }
    public static hasNotOwn(key: Key, onwer: Metadata.Owner): boolean {
        return this.of(onwer).hasNotOwn(key);
    }

    public static has(key: Key, onwer: Metadata.Owner): boolean {
        return this.of(onwer).has(key);
    }
    public static hasNot(key: Key, onwer: Metadata.Owner): boolean {
        return this.of(onwer).hasNot(key);
    }

    public static getOwn(key: Key, onwer: Metadata.Owner): any {
        return this.of(onwer).getOwn(key);
    }
    public static get(key: Key, onwer: Metadata.Owner): any {
        return this.of(onwer).get(key);
    }

    public static set(key: Key, value: unknown, onwer: Metadata.Owner): void {
        this.of(onwer).set(key, value);
    }

    public static delete(key: Key, onwer: Metadata.Owner): void {
        this.of(onwer).delete(key);
    }

    private readonly data: Map<Key, unknown>;

    private constructor(
        public readonly owner: Metadata.Owner,
        public readonly parent: null | Metadata = null
    ) {
        this.data = new Map();
    }

    public of(parameterIndex: number): Metadata {
        if (Check.isNotUndefined(this.owner.parameterIndex)) {
            throw new Error('Parameter metadata can only be used in constructor or method metadata.');
        }

        if (Check.isNotInteger(parameterIndex) || Check.less(parameterIndex, 0)) {
            throw new Error('Invalid parameter index.');
        }

        const parentParameterMetadata: null | Metadata = Check.isNotNull(this.parent)
            ? this.parent.of(parameterIndex)
            : null;
        const parametersMetadata: Array<undefined | Metadata> = this.hasOwn(PARAMETERS_METADATA_KEY)
            ? this.getOwn(PARAMETERS_METADATA_KEY)
            : [];
        const parameterMetadata: Metadata = parametersMetadata[parameterIndex] ?? new Metadata({ ...this.owner, parameterIndex }, parentParameterMetadata);

        parametersMetadata[parameterIndex] = parameterMetadata;

        this.set(PARAMETERS_METADATA_KEY, parametersMetadata);

        return parameterMetadata;
    }

    public hasOwn(key: Key): boolean {
        return this.data.has(key);
    }
    public hasNotOwn(key: Key): boolean {
        return Check.isFalse(this.hasOwn(key));
    }

    public has(key: Key): boolean {
        return this.hasOwn(key) || Check.isNotNull(this.parent) && this.parent.has(key);
    }
    public hasNot(key: Key): boolean {
        return Check.isFalse(this.has(key));
    }

    public getOwn(key: Key): any {
        if (this.hasNotOwn(key)) {
            throw new Error('Key does not exist.');
        }

        return this.data.get(key);
    }
    public get(key: Key): any {
        if (this.hasOwn(key)) {
            return this.getOwn(key);
        }

        if (Check.isNull(this.parent)) {
            throw new Error('Key does not exist.');
        }

        return this.parent.get(key);
    }

    public set(key: Key, value: unknown): void {
        this.data.set(key, value);
    }

    public delete(key: Key): void {
        this.data.delete(key);
    }
}
export namespace Metadata {
    export interface Owner {
        readonly target: object;
        readonly propertyKey?: Key;
        readonly parameterIndex?: number;
    }

    export type Decorator = ClassDecorator & PropertyDecorator & MethodDecorator & ParameterDecorator;
    export namespace Decorator {
        export type Handler = (metadata: Metadata) => void;

        export function create(handler: Handler): Decorator {
            return (target: object, propertyKey?: Key, descriptorOrParameterIndex?: PropertyDescriptor | number): void => {
                const parameterIndex: undefined | number = Check.isNumber(descriptorOrParameterIndex)
                    ? descriptorOrParameterIndex
                    : undefined;
                const metadata: Metadata = Metadata.of({
                    target,
                    propertyKey,
                    parameterIndex
                });

                handler(metadata);
            };
        }
    }

    export function Define(key: Key, value: unknown): Decorator {
        return Decorator.create((metadata: Metadata): void => metadata.set(key, value));
    }
}
