import { Check } from '@typemon/check';
//
import { Key } from './key';
//
//
//
export namespace Reflection {
    export function hasOwnMetadata(key: Key, target: object, propertyKey?: Key): boolean {
        return Reflect.hasOwnMetadata(key, target, propertyKey as Key);
    }
    export function hasNotOwnMetadata(key: Key, target: object, propertyKey?: Key): boolean {
        return Check.isFalse(Reflection.hasOwnMetadata(key, target, propertyKey));
    }

    export function hasMetadata(key: Key, target: object, propertyKey?: Key): boolean {
        return Reflect.hasMetadata(key, target, propertyKey as Key);
    }
    export function hasNotMetadata(key: Key, target: object, propertyKey?: Key): boolean {
        return Check.isFalse(Reflection.hasMetadata(key, target, propertyKey));
    }

    export function getOwnMetadata(key: Key, target: object, propertyKey?: Key): any {
        if (Reflection.hasNotOwnMetadata(key, target, propertyKey)) {
            throw new Error('Key does not exist.');
        }

        return Reflect.getOwnMetadata(key, target, propertyKey as Key);
    }
    export function getMetadata(key: Key, target: object, propertyKey?: Key): any {
        if (Reflection.hasNotMetadata(key, target, propertyKey)) {
            throw new Error('Key does not exist.');
        }

        return Reflect.getMetadata(key, target, propertyKey as Key);
    }

    export function setMetadata(key: Key, value: unknown, target: object, propertyKey?: Key): void {
        Reflect.defineMetadata(key, value, target, propertyKey as Key);
    }

    export function deleteMetadata(key: Key, target: object, propertyKey?: Key): void {
        Reflect.deleteMetadata(key, target, propertyKey as Key);
    }

    export interface DecorationTarget {
        readonly target: object;
        readonly propertyKey?: Key;
        readonly parameterIndex?: number;
    }

    export function decorate(decorators: ReadonlyArray<ClassDecorator | PropertyDecorator | MethodDecorator | ParameterDecorator>, { target, propertyKey, parameterIndex }: DecorationTarget): void {
        if (Check.equal(decorators.length, 0)) {
            throw new Error('Provide at least one decorator.');
        }

        if (Check.isUndefined(parameterIndex)) {
            Reflect.decorate(decorators as any, target, propertyKey as any);
        }
        else {
            for (const decorator of decorators.slice().reverse() as ReadonlyArray<any>) {
                decorator(target, propertyKey, parameterIndex);
            }
        }
    }

    export function DefineMetadata(key: Key, value: unknown): ClassDecorator & PropertyDecorator & MethodDecorator {
        return (target: object, propertyKey?: Key): void => {
            Reflection.setMetadata(key, value, target, propertyKey);
        };
    }
}
