# Reflection - [![version](https://img.shields.io/npm/v/@typemon/reflection.svg)](https://www.npmjs.com/package/@typemon/reflection) [![license](https://img.shields.io/npm/l/@typemon/reflection.svg)](https://gitlab.com/monster-space-network/typemon/reflection/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/reflection/dev/typescript.svg)
- Parameter metadata management
- Inheritance support
- Easy integration with decorators



## Installation
```
$ npm install @typemon/reflection reflect-metadata
```

#### [TypeScript](https://github.com/Microsoft/TypeScript)
- Version **`3.5 or later`** is required.
- Configure the decorator and metadata options.
    - https://www.typescriptlang.org/docs/handbook/decorators.html#metadata
```json
{
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true
}
```

#### [Reflect Metadata](https://github.com/rbuckton/reflect-metadata)
- Import only once at application entry point.
```typescript
import 'reflect-metadata';
```



## Usage
- [Reflection](#reflection)
- [Metadata](#metadata)
- [Metadata Decorator](#metadata-decorator)

### Reflection
```typescript
Reflection.hasOwnMetadata(metadataKey, target, propertyKey?): boolean;
Reflection.hasNotOwnMetadata(metadataKey, target, propertyKey?): boolean;
Reflection.hasMetadata(metadataKey, target, propertyKey?): boolean;
Reflection.hasMetadata(metadataKey, target, propertyKey?): boolean;
Reflection.getOwnMetadata(metadataKey, target, propertyKey?): any;
Reflection.getMetadata(metadataKey, target, propertyKey?): any;
Reflection.setMetadata(metadataKey, metadataValue, target, propertyKey?): void;
Reflection.deleteMetadata(metadataKey, target, propertyKey?): void;
Reflection.decorate(decorators, {
    target,
    propertyKey?,
    parameterIndex?
}): void;
Reflection.DefineMetadata(metadataKey, metadataValue): ClassDecorator & PropertyDecorator & MethodDecorator;
```

### Metadata
```typescript
Metadata.of({
    target,
    propertyKey?,
    parameterIndex?
}): Metadata;
Metadata.hasOwn(metadataKey, {
    target,
    propertyKey?,
    parameterIndex?
}): boolean;
Metadata.hasNotOwn(metadataKey, {
    target,
    propertyKey?,
    parameterIndex?
}): boolean;
Metadata.has(metadataKey, {
    target,
    propertyKey?,
    parameterIndex?
}): boolean;
Metadata.hasNot(metadataKey, {
    target,
    propertyKey?,
    parameterIndex?
}): boolean;
Metadata.getOwn(metadataKey, {
    target,
    propertyKey?,
    parameterIndex?
}): any;
Metadata.get(metadataKey, {
    target,
    propertyKey?,
    parameterIndex?
}): any;
Metadata.set(metadataKey, metadataValue, {
    target,
    propertyKey?,
    parameterIndex?
}): void;
Metadata.delete(metadataKey, {
    target,
    propertyKey?,
    parameterIndex?
}): void;
```

### Metadata Decorator
You can easily set metadata using the `Metadata.Define` decorator.
```typescript
@Metadata.Define(metadataKey, metadataValue)
class Target {
    @Metadata.Define(metadataKey, metadataValue)
    public static staticProperty: unknown;

    @Metadata.Define(metadataKey, metadataValue)
    public static staticMethod(
        @Metadata.Define(metadataKey, metadataValue)
        staticMethodParameter: unknown
    ): void { }

    @Metadata.Define(metadataKey, metadataValue)
    public instanceProperty: unknown;

    public constructor(
        @Metadata.Define(metadataKey, metadataValue)
        constructorParameter: unknown
    ) { }

    @Metadata.Define(metadataKey, metadataValue)
    public instanceMethod(
        @Metadata.Define(metadataKey, metadataValue)
        instanceMethodParameter: unknown
    ): void { }
}
```
You can create and use custom decorators as needed.
```typescript
function CustomDecorator(): Metadata.Decorator {
    return Metadata.Decorator.create((metadata: Metadata): void => {
        . . .
    });
}
```
